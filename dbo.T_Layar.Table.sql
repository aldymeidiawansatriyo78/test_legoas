USE [Legoas]
GO
/****** Object:  Table [dbo].[T_Layar]    Script Date: 02/04/2023 23:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Layar](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[akun_id] [bigint] NULL,
	[layar_id] [bigint] NULL,
 CONSTRAINT [PK_T_Layar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_Layar]  WITH CHECK ADD  CONSTRAINT [FK_T_Layar_Layar] FOREIGN KEY([layar_id])
REFERENCES [dbo].[Layar] ([id])
GO
ALTER TABLE [dbo].[T_Layar] CHECK CONSTRAINT [FK_T_Layar_Layar]
GO
ALTER TABLE [dbo].[T_Layar]  WITH CHECK ADD  CONSTRAINT [FK_T_Layar_T_Layar] FOREIGN KEY([id])
REFERENCES [dbo].[T_Layar] ([id])
GO
ALTER TABLE [dbo].[T_Layar] CHECK CONSTRAINT [FK_T_Layar_T_Layar]
GO
