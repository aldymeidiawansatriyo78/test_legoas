USE [Legoas]
GO
/****** Object:  Table [dbo].[Akun]    Script Date: 02/04/2023 23:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Akun](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nama_akun] [varchar](100) NULL,
	[password] [nvarchar](50) NULL,
	[tgl_daftar] [datetime] NULL,
 CONSTRAINT [PK_Akun] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
