USE [Legoas]
GO
/****** Object:  Table [dbo].[Pengguna]    Script Date: 02/04/2023 23:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pengguna](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nama_pengguna] [varchar](200) NULL,
	[alamat] [varchar](500) NULL,
	[kode_pos] [varchar](50) NULL,
	[provinsi] [varchar](100) NULL,
	[kantor] [varchar](100) NULL,
	[akun_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Pengguna] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Pengguna]  WITH CHECK ADD  CONSTRAINT [FK_Pengguna_Akun] FOREIGN KEY([akun_id])
REFERENCES [dbo].[Akun] ([id])
GO
ALTER TABLE [dbo].[Pengguna] CHECK CONSTRAINT [FK_Pengguna_Akun]
GO
