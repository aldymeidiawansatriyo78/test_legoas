﻿using System;

namespace Legoas_Test.DTOs
{
    public class DaftarAkunDTO
    {
        public string namaAkun { get; set; }

        public DateTime? TglDaftar { get; set; } 

        public string namaPengguna { get; set; }    

        public string peran { get; set; }
    }
}
