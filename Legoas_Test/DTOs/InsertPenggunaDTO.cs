﻿using System;
using System.Linq;

namespace Legoas_Test.DTOs
{
    public class InsertPenggunaDTO
    {
        public string namaPengguna { get; set; }

        public string alamat { get; set; }

        public string kodePos { get; set; }

        public string provinsi { get; set; }

        public int[] kantors { get; set; }

        public int akunId { get; set; }

        public bool HasValues
        {
            get
            {
                return !string.IsNullOrEmpty(namaPengguna) ||
                    !string.IsNullOrEmpty(alamat) ||
                    !string.IsNullOrEmpty(kodePos) ||
                    !string.IsNullOrEmpty(provinsi);
            }
        }

        
    }
}
