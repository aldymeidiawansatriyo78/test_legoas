﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class TKantor
    {
        public long Id { get; set; }
        public long? AkunId { get; set; }
        public long? KantorId { get; set; }

        public virtual Akun Akun { get; set; }
        public virtual Kantor Kantor { get; set; }
    }
}
