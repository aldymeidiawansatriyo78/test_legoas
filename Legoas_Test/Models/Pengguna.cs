﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class Pengguna
    {
        public long Id { get; set; }
        public string NamaPengguna { get; set; }
        public string Alamat { get; set; }
        public string KodePos { get; set; }
        public string Provinsi { get; set; }
        public string Kantor { get; set; }
        public long AkunId { get; set; }

        public virtual Akun Akun { get; set; }
    }
}
