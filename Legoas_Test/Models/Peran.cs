﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class Peran
    {
        public Peran()
        {
            TPerans = new HashSet<TPeran>();
        }

        public long Id { get; set; }
        public string KodePeran { get; set; }
        public string NamaPeran { get; set; }

        public virtual ICollection<TPeran> TPerans { get; set; }
    }
}
