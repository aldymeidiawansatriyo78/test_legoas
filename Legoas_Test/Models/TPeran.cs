﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class TPeran
    {
        public long Id { get; set; }
        public long? AkunId { get; set; }
        public long? PeranId { get; set; }

        public virtual Akun Akun { get; set; }
        public virtual Peran Peran { get; set; }
    }
}
