﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class TLayar
    {
        public long Id { get; set; }
        public long? AkunId { get; set; }
        public long? LayarId { get; set; }

        public virtual Layar Layar { get; set; }
    }
}
