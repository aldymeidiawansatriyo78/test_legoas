﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Legoas_Test.Models
{
    public partial class LegoasContext : DbContext
    {
        public LegoasContext()
        {
        }

        public LegoasContext(DbContextOptions<LegoasContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Akun> Akuns { get; set; }
        public virtual DbSet<Kantor> Kantors { get; set; }
        public virtual DbSet<Layar> Layars { get; set; }
        public virtual DbSet<Pengguna> Penggunas { get; set; }
        public virtual DbSet<Peran> Perans { get; set; }
        public virtual DbSet<TKantor> TKantors { get; set; }
        public virtual DbSet<TLayar> TLayars { get; set; }
        public virtual DbSet<TPeran> TPerans { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server = .\\MSQLSERVER01; Database = Legoas; User Id = nodeJS; Password = Passwd789;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Akun>(entity =>
            {
                entity.ToTable("Akun");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NamaAkun)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nama_akun");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .HasColumnName("password");

                entity.Property(e => e.TglDaftar)
                    .HasColumnType("datetime")
                    .HasColumnName("tgl_daftar");
            });

            modelBuilder.Entity<Kantor>(entity =>
            {
                entity.ToTable("Kantor");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.KodeCabang)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("kode_cabang");

                entity.Property(e => e.NamaCabang)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nama_cabang");
            });

            modelBuilder.Entity<Layar>(entity =>
            {
                entity.ToTable("Layar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.KodeLayar)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("kode_layar");

                entity.Property(e => e.NamaLayar)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nama_layar");
            });

            modelBuilder.Entity<Pengguna>(entity =>
            {
                entity.ToTable("Pengguna");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AkunId).HasColumnName("akun_id");

                entity.Property(e => e.Alamat)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("alamat");

                entity.Property(e => e.Kantor)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("kantor");

                entity.Property(e => e.KodePos)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("kode_pos");

                entity.Property(e => e.NamaPengguna)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("nama_pengguna");

                entity.Property(e => e.Provinsi)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("provinsi");

                entity.HasOne(d => d.Akun)
                    .WithMany(p => p.Penggunas)
                    .HasForeignKey(d => d.AkunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pengguna_Akun");
            });

            modelBuilder.Entity<Peran>(entity =>
            {
                entity.ToTable("Peran");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.KodePeran)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("kode_peran");

                entity.Property(e => e.NamaPeran)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nama_peran");
            });

            modelBuilder.Entity<TKantor>(entity =>
            {
                entity.ToTable("T_Kantor");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AkunId).HasColumnName("akun_id");

                entity.Property(e => e.KantorId).HasColumnName("kantor_id");

                entity.HasOne(d => d.Akun)
                    .WithMany(p => p.TKantors)
                    .HasForeignKey(d => d.AkunId)
                    .HasConstraintName("FK_T_Kantor_Akun");

                entity.HasOne(d => d.Kantor)
                    .WithMany(p => p.TKantors)
                    .HasForeignKey(d => d.KantorId)
                    .HasConstraintName("FK_T_Kantor_Kantor");
            });

            modelBuilder.Entity<TLayar>(entity =>
            {
                entity.ToTable("T_Layar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AkunId).HasColumnName("akun_id");

                entity.Property(e => e.LayarId).HasColumnName("layar_id");

                entity.HasOne(d => d.Layar)
                    .WithMany(p => p.TLayars)
                    .HasForeignKey(d => d.LayarId)
                    .HasConstraintName("FK_T_Layar_Layar");
            });

            modelBuilder.Entity<TPeran>(entity =>
            {
                entity.ToTable("T_Peran");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AkunId).HasColumnName("akun_id");

                entity.Property(e => e.PeranId).HasColumnName("peran_id");

                entity.HasOne(d => d.Akun)
                    .WithMany(p => p.TPerans)
                    .HasForeignKey(d => d.AkunId)
                    .HasConstraintName("FK_T_Peran_Akun");

                entity.HasOne(d => d.Peran)
                    .WithMany(p => p.TPerans)
                    .HasForeignKey(d => d.PeranId)
                    .HasConstraintName("FK_T_Peran_Peran");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
