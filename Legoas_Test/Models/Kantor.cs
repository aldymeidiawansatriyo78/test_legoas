﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class Kantor
    {
        public Kantor()
        {
            TKantors = new HashSet<TKantor>();
        }

        public long Id { get; set; }
        public string KodeCabang { get; set; }
        public string NamaCabang { get; set; }

        public virtual ICollection<TKantor> TKantors { get; set; }
    }
}
