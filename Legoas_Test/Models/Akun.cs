﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class Akun
    {
        public Akun()
        {
            Penggunas = new HashSet<Pengguna>();
            TKantors = new HashSet<TKantor>();
            TPerans = new HashSet<TPeran>();
        }

        public long Id { get; set; }
        public string NamaAkun { get; set; }
        public string Password { get; set; }
        public DateTime? TglDaftar { get; set; }

        public virtual ICollection<Pengguna> Penggunas { get; set; }
        public virtual ICollection<TKantor> TKantors { get; set; }
        public virtual ICollection<TPeran> TPerans { get; set; }
    }
}
