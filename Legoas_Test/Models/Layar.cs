﻿using System;
using System.Collections.Generic;

namespace Legoas_Test.Models
{
    public partial class Layar
    {
        public Layar()
        {
            TLayars = new HashSet<TLayar>();
        }

        public long Id { get; set; }
        public string KodeLayar { get; set; }
        public string NamaLayar { get; set; }

        public virtual ICollection<TLayar> TLayars { get; set; }
    }
}
