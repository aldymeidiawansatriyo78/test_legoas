﻿using Legoas_Test.DTOs;
using System.Collections.Generic;

namespace Legoas_Test.Interfaces
{
    public interface IDaftarAkunProvider
    {
        IEnumerable<DaftarAkunDTO> GetPeran();
    }
}
