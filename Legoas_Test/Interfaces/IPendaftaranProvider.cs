﻿using Legoas_Test.DTOs;
using Legoas_Test.Models;
using System.Collections.Generic;

namespace Legoas_Test.Interfaces
{
    public interface IPendaftaranProvider
    {
        IEnumerable<Peran> GetPeran();
        IEnumerable<Kantor> GetKantor();
        IEnumerable<Layar> GetLayar();
        int InsertInitialPendaftaran(string namaAkun, string password, int[] perans);
        bool CheckIfAkunExists(string namaAkun);
        int InsertLayar(int id, int[] layars);
        int InsertPengguna(InsertPenggunaDTO pengguna);
    }


}
