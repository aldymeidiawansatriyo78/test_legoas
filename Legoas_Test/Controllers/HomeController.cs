﻿//using Legoas_Test.Models;
using Legoas_Test.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Legoas_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPendaftaranProvider _pendaftaranProvider;

        public HomeController(ILogger<HomeController> logger, IPendaftaranProvider pendaftaranProvider)
        {
            _logger = logger;
            _pendaftaranProvider = pendaftaranProvider;
        }

        public IActionResult Index()
        {
            var test = _pendaftaranProvider.GetPeran();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
