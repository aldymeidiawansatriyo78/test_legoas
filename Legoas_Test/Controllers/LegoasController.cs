﻿using Legoas_Test.DTOs;
using Legoas_Test.Interfaces;
using Legoas_Test.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Legoas_Test.Controllers
{
    public class LegoasController : Controller
    {

        private readonly ILogger<HomeController> _logger;
        private readonly IPendaftaranProvider _pendaftaranProvider;
        private readonly IDaftarAkunProvider _daftarAkunProvider;

        public LegoasController(ILogger<HomeController> logger, IPendaftaranProvider pendaftaranProvider, IDaftarAkunProvider daftarAkunProvider)
        {
            _logger = logger;
            _pendaftaranProvider = pendaftaranProvider;
            _daftarAkunProvider = daftarAkunProvider;
        }

        // GET: LegoasController
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Pendaftaran()
        {
            var vm = new PendaftaranViewModel
            {              
                Perans = _pendaftaranProvider.GetPeran()
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult InsertPeran(string namaAkun, string password, int[] perans)
        {
            var checkAkuns = _pendaftaranProvider.CheckIfAkunExists(namaAkun);

            if(!checkAkuns) { return Json(new { id = 0 }); }

            int resultInitial = _pendaftaranProvider.InsertInitialPendaftaran(namaAkun, password, perans);

            return Json(new { id = resultInitial });
        }
        
        [HttpPost]
        public IActionResult Pengguna(int id)
        {
            var vm = new PenggunaViewModel
            {
                id = id,
                Kantors = _pendaftaranProvider.GetKantor()
            };

            return View(vm);  
        }

        [HttpPost]
        public IActionResult InsertPengguna(InsertPenggunaDTO pengguna)
        {
           int resultPengguna = _pendaftaranProvider.InsertPengguna(pengguna);

           return Json(new { id = resultPengguna});
        }

        [HttpPost]
        public IActionResult Layar(int id)
        {
            var vm = new LayarViewModel
            {
                id = id,
                Layars = _pendaftaranProvider.GetLayar()
            };
            return View(vm);
        }

        [HttpPost]
        public IActionResult InsertLayar(int id, int[] layars)
        {
            string result = "Success";

            int resultLayar = _pendaftaranProvider.InsertLayar(id,layars);

            if (resultLayar == 0) result = "Failed"; 

            return Json(new { message = result });
        }

        [HttpGet]
        public IActionResult DaftarAkun()
        {
            var daftarAkun = _daftarAkunProvider.GetPeran();
            var vm = new DaftarAkunViewModel
            {
                DaftarAkun = daftarAkun
            };

            return View(vm);
        }







        //// GET: LegoasController/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: LegoasController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: LegoasController/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: LegoasController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: LegoasController/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: LegoasController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: LegoasController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
