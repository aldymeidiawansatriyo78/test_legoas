﻿using Legoas_Test.DTOs;
using Legoas_Test.Interfaces;
using Legoas_Test.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;


namespace Legoas_Test.Providers
{
    public class PendaftaranProvider : IPendaftaranProvider
    {
        private readonly LegoasContext _context;

        public PendaftaranProvider(LegoasContext db)
        {
            _context = db;
        }

        public IEnumerable<Peran> GetPeran()
        {
            return _context.Perans.ToList();
        }

        public IEnumerable<Kantor> GetKantor()
        {
            return _context.Kantors.ToList();
        }

        public IEnumerable<Layar> GetLayar()
        {
            return _context.Layars.ToList();
        }

        public bool CheckIfAkunExists(string namaAkun)
        {
            var check = _context.Akuns.Where(a => a.NamaAkun == namaAkun).FirstOrDefault();

            if (check != null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public int InsertInitialPendaftaran(string namaAkun, string password, int[] perans)
        {
            var akuns = new Akun()
            {
                NamaAkun = namaAkun,
                Password = password,
                TglDaftar = DateTime.Now
            };

            _context.Akuns.Add(akuns);

            _context.SaveChanges();

            int id = Convert.ToInt32(akuns.Id);

            List<TPeran> tperans = new List<TPeran>();

            foreach (var item in perans)
            {
                var tperan = new TPeran();  
                tperan.AkunId = id;
                tperan.PeranId = item;
                tperans.Add(tperan);
            }

            _context.TPerans.AddRange(tperans);
            _context.SaveChanges();

            return id;
        }

        public int InsertPengguna(InsertPenggunaDTO pengguna)
        {
            var penggunas = new Pengguna()
            {
                NamaPengguna = pengguna.namaPengguna,
                Alamat = pengguna.alamat,
                KodePos = pengguna.kodePos,
                Provinsi = pengguna.provinsi,
                AkunId = pengguna.akunId
            };

            _context.Penggunas.Add(penggunas);
            _context.SaveChanges();

            if (pengguna.kantors == null)
            {
                pengguna.kantors = Array.Empty<int>();
            }
    
            if (!pengguna.HasValues && pengguna.kantors.Count() == 0) return pengguna.akunId;

            List<TKantor> tkantors = new List<TKantor>();

            foreach (var item in pengguna.kantors)
            {
                var tkantor = new TKantor()
                {
                    AkunId = pengguna.akunId,
                    KantorId = item
                };

                tkantors.Add(tkantor);
            }

            _context.TKantors.AddRange(tkantors);
            _context.SaveChanges();

            return pengguna.akunId;
        }

        public int InsertLayar(int id, int[] layars)
        {
            List<TLayar> tlayars = new List<TLayar>();

            foreach (var item in layars)
            {
                var tlayar = new TLayar()
                {
                    AkunId = id,
                    LayarId = item
                };

                tlayars.Add(tlayar);
            }

            _context.TLayars.AddRange(tlayars);
            _context.SaveChanges();

            return id;
        }
    }
}
