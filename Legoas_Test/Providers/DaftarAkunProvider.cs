﻿using Legoas_Test.DTOs;
using Legoas_Test.Interfaces;
using Legoas_Test.Models;
using System.Collections.Generic;
using System.Linq;

namespace Legoas_Test.Providers
{
    public class DaftarAkunProvider : IDaftarAkunProvider
    {
        private readonly LegoasContext _context;

        public DaftarAkunProvider(LegoasContext db)
        {
            _context = db;
        }

        public IEnumerable<DaftarAkunDTO> GetPeran()
        {
            IEnumerable<DaftarAkunDTO> listPeran = new List<DaftarAkunDTO>();

            listPeran = (from akuns in _context.Akuns
                            join penggunas in _context.Penggunas on akuns.Id equals penggunas.AkunId
                            join tperans in _context.TPerans on akuns.Id equals tperans.AkunId
                            join perans in _context.Perans on tperans.PeranId equals perans.Id
                            select new DaftarAkunDTO
                            {
                                namaAkun = akuns.NamaAkun,
                                TglDaftar = akuns.TglDaftar,
                                namaPengguna = penggunas.NamaPengguna,
                                peran = perans.NamaPeran
                            }).ToList();

            //List<DaftarAkunDTO> daftarAkuns = new List<DaftarAkunDTO>();

            //foreach(var item in listPeran)
            //{
            //    var daftarAkun = new DaftarAkunDTO();

            //    daftarAkun.namaAkun = item.namaAkun;
            //    daftarAkun.TglDaftar = item.TglDaftar;
            //    daftarAkun.namaPengguna = item.namaPengguna;
            //    daftarAkun.peran = item.peran;

            //    daftarAkuns.Add(daftarAkun);
            //}

            return listPeran;
        }
    }
}
