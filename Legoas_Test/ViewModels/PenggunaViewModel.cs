﻿using Legoas_Test.Models;
using System.Collections.Generic;

namespace Legoas_Test.ViewModels
{
    public class PenggunaViewModel
    {
        public int id { get; set; }

        public IEnumerable<Kantor> Kantors { get; set; }
    }
}
