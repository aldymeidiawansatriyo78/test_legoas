﻿using Legoas_Test.DTOs;
using System.Collections;
using System.Collections.Generic;

namespace Legoas_Test.ViewModels
{
    public class DaftarAkunViewModel
    {
        public IEnumerable<DaftarAkunDTO> DaftarAkun { get; set; }   
    }
}
