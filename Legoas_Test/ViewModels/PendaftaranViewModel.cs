﻿using Legoas_Test.DTOs;
using Legoas_Test.Models;
using System.Collections;
using System.Collections.Generic;

namespace Legoas_Test.ViewModels
{
    public class PendaftaranViewModel
    {
        public IEnumerable<Peran> Perans { get; set; }
    }
}
