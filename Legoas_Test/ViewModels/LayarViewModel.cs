﻿using Legoas_Test.Models;
using System.Collections.Generic;

namespace Legoas_Test.ViewModels
{
    public class LayarViewModel
    {
        public int id { get; set; }           

        public IEnumerable<Layar> Layars { get; set; }
    }
}
