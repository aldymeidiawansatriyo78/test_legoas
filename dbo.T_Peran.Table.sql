USE [Legoas]
GO
/****** Object:  Table [dbo].[T_Peran]    Script Date: 02/04/2023 23:50:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Peran](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[akun_id] [bigint] NULL,
	[peran_id] [bigint] NULL,
 CONSTRAINT [PK_T_Peran] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_Peran]  WITH CHECK ADD  CONSTRAINT [FK_T_Peran_Akun] FOREIGN KEY([akun_id])
REFERENCES [dbo].[Akun] ([id])
GO
ALTER TABLE [dbo].[T_Peran] CHECK CONSTRAINT [FK_T_Peran_Akun]
GO
ALTER TABLE [dbo].[T_Peran]  WITH CHECK ADD  CONSTRAINT [FK_T_Peran_Peran] FOREIGN KEY([peran_id])
REFERENCES [dbo].[Peran] ([id])
GO
ALTER TABLE [dbo].[T_Peran] CHECK CONSTRAINT [FK_T_Peran_Peran]
GO
